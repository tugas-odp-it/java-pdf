package com.javapdf.controller;

import com.javapdf.entity.User;
import com.javapdf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    @Autowired
    UserService userService;
    @GetMapping("/user")
    public User getById(@RequestParam Integer id){
        return userService.getById(id);
    }
    @PostMapping("/user")
    public User addParameter(@RequestBody User user){
        return userService.addUser(user);
    }
}
